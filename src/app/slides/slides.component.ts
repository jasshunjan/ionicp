import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import SwiperCore, { EffectFade,Autoplay, Keyboard, Pagination, Scrollbar, Zoom } from 'swiper';

SwiperCore.use([Autoplay, Keyboard, Pagination, Scrollbar, Zoom]);

// import { IonicSlides } from '@ionic/angular';

// SwiperCore.use([EffectFade, IonicSlides]);

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SlidesComponent implements OnInit {

  private slides: any;

  constructor() { }
  

  ngOnInit() {}

}
